# Ember Wishlist #

Sample Ember.js Wishlist application

### What is this repository for? ###


### Requirements ###

* Node.js
* NPM
* GIT
* Bower
* grunt-cli

### Before starting ###

* run: npm install && bower install

### Run application ###

* run: grunt serve
* browse [http://localhost:9000](http://localhost:9000)
