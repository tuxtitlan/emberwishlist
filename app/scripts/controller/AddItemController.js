App.AddItemController = Em.ObjectController.extend({
  iAm : 'App.AddItemController',
  itemFormController: null,

  init : function(){
    var itemFormController = App.ItemFormController.create({
      isAdd : true
    });
    this._super();
    this.set('itemFormController', itemFormController);
  }
});
