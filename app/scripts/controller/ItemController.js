App.ItemController = Em.ObjectController.extend({
  iAm : 'App.ItemController',
  itemFormController : null,

  init : function(){
    var itemFormController = App.ItemFormController.create({
      isAdd : false
    });

    this._super();
    this.set('itemFormController', itemFormController);
  }
});
