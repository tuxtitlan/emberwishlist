App.MainController = Em.ObjectController.extend({
  iAm : 'App.MainController',
  addItemController: null,
  itemListController: null,

  init : function(){
    var itemListController = App.ItemListController.create();
    var addItemController = App.AddItemController.create();
    this._super();
    this.setProperties({
      itemListController: itemListController,
      addItemController: addItemController
    });
  }
});
