window.App = Ember.Application.create({
  LOG_VIEW_LOOKUPS: true
});

App.ready = function(){
  this.mainController = App.MainController.create();
};
