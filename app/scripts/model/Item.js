App.Item = DS.Model.extend({
  id: DS.attr('number'),
  name: DS.attr('string'),
  category: DS.attr('string')
});
