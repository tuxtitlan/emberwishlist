App.AddItemView = Em.View.extend({
  iAm: 'App.AddItemView',
  templateName : 'add-item-template',
  classNames : ['row'],
  showForm: false,

  init: function(){
    this._super();
    this.set('showForm', false);
  },

  actions:{
    toggleForm: function(){
      this.toggleProperty('showForm');
    }
  }
});
