App.FooterView = Em.View.extend({
  iAm: 'App.FooterView',
  templateName : 'footer-template',
  classNames : ['row', 'footer'],

  init: function(){
    this._super();
  }
});
