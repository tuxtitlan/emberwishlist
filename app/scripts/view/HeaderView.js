App.HeaderView = Em.View.extend({
  iAm: 'App.HeaderView',
  templateName : 'header-template',
  classNames : ['row','header'],

  init: function(){
    this._super();
  }
});
