App.ItemFormView = Em.View.extend({
  iAm: 'App.ItemFormView',
  templateName : 'item-form-template',
  classNames : ['modal', 'fade'],
  tabIndex : "-1",

  init: function(){
    this._super();
  },

  didInsertElement : function(){
    this.set('controller.elementId', this.get('elementId'));
  }
});
