App.MainView = Em.View.extend({
  iAm: 'App.MainView',
  templateName : 'main-template',
  classNames : ['container','main'],

  init: function(){
    this._super();
  }
});
